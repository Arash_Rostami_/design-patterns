<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "welcome";
});

// creational design patterns
$router->get('/builder', 'DesignPatternsController@indexBuilder');
$router->get('/factory', 'DesignPatternsController@indexFactory');
$router->get('/singleton', 'DesignPatternsController@indexSingleton');

// structural design patterns
$router->get('/bridge', 'DesignPatternsController@indexBridge');
$router->get('/adapter', 'DesignPatternsController@indexAdapter');
$router->get('/facade', 'DesignPatternsController@indexFacade');
$router->get('/decorator', 'DesignPatternsController@indexDecorator');
$router->get('/proxy', 'DesignPatternsController@indexroxy');

// Behavioral design patterns
$router->get('/iterator', 'DesignPatternsController@indexIterator');
$router->get('/observer', 'DesignPatternsController@indexObserver');
$router->get('/visitor', 'DesignPatternsController@indexVisitor');
$router->get('/mediator', 'DesignPatternsController@indexMediator');
$router->get('/strategy', 'DesignPatternsController@indexStrategy');




// response to test
$router->get('/test', 'DesignPatternsController@indexTest');
$router->get('/magic', 'DesignPatternsController@indexMagic');
$router->get('/bits', 'DesignPatternsController@indexBits');




