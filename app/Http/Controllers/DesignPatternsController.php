<?php

namespace App\Http\Controllers;

class DesignPatternsController extends Controller
{
    public function indexBuilder()
    {
        return view('builder.index');
    }

    public function indexFactory()
    {
        return view('factory.index');
    }

    public function indexSingleton()
    {
        return view('singleton.index');
    }


    public function indexBridge()
    {
        return view('bridge.index');
    }

    public function indexAdapter()
    {
        return view('adapter.index');
    }

    public function indexFacade()
    {
        return view('facade.index');
    }

    public function indexDecorator()
    {
        return view('decorator.index');
    }

    public function indexProxy()
    {
        return view('proxy.index');
    }

    public function indexIterator()
    {
        return view('iterator.index');
    }

    public function indexObserver()
    {
        return view('observer.index');
    }

    public function indexVisitor()
    {
        return view('visitor.index');
    }
    public function indexMediator()
    {
        return view('mediator.index');
    }

    public function indexStrategy()
    {
        return view('strategy.index');
    }






    public function indexTest()
    {
        return view('adapter.test');
    }

    public function indexBits()
    {
        return view('adapter.bits');
    }

    public function indexMagic()
    {
        return view('adapter.magic');
    }
}
