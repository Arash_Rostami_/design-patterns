<?php


// getting API service
class CarPriceAPI
{
    public function __construct()
    {
        echo '⏳ connecting to API service  ... ';
    }

    public function getPrice($type): string
    {
        $price = [
            "Porsche" => "$80'000",
            "Mercedes" => "$60'000",
            "BMW" => "$50'000",
            "AUDI" => "$40'000",
            "VW" => "$30'000",
            "OPEL" => "$25'000",
        ];
        return $price[$type];
    }
}

// proxy to get and/or cache API service
class CarPriceAPIProxy
{
    protected $cache;

    public function __construct()
    {
        $this->cache = [];
    }

    public function getPrice($type)
    {
        echo (!empty($this->cache[$type]))
            ?
            "... using cached service ✔  {$this->cache[$type]}"
            :
            $this->cache[$type] = (new CarPriceAPI())->getPrice($type);
    }
}

(function () {
//show result
    $api = new CarPriceAPIProxy();

    $api->getPrice('BMW');
    $api->getPrice('BMW');
    $api->getPrice('VW');
    $api->getPrice('VW');
    $api->getPrice('VW');
})();


// JavaScript equivalent

echo '



<script>
// getting API service
class CarPriceAPI {
    getPrice(type) {
        console.warn("⏳ connecting to API service ...");

        let price = {
            "Porsche": "$80\'000",
            "Mercedes": "$60\'000",
            "BMW": "$50\'000",
            "AUDI": "$40\'000",
            "VW": "$30\'000",
            "OPEL": "$25\'000",
        }
        return price[type];
    }
}

// proxy to get and/or cache API service
class CarPriceAPIProxy {
    constructor() {
        this.cache = {};
    }

    getPrice(type) {

        if (this.cache[type]) {
            console.info(" ... using cached service ✔ ");
            return this.cache[type];
        }
        this.cache[type] = new CarPriceAPI().getPrice(type);
        return this.cache[type];
    }
}

(function useApi() {
//show result
    let api = new CarPriceAPIProxy();

    console.log(api.getPrice("BMW"));
    console.log(api.getPrice("BMW"));
    console.log(api.getPrice("VW"));
    console.log(api.getPrice("VW"));
    console.log(api.getPrice("VW"));
})();

    </script>
    ';

