<?php

// Abstract to modify configs
abstract class CameraConfigs
{
    public $name, $pixel, $quality;

    public function __construct($name, $pixel, $quality)
    {
        $this->name = $name;
        $this->pixel = $pixel;
        $this->quality = $quality;
    }

    abstract public function magnify();

    abstract public function reset();

    abstract public function videoRecord();
}

// Common features for DRY purpose
trait Features
{
    public function magnify()
    {
        echo "{$this->name} zooming in on {$this->pixel}X...";
    }

    public function reset()
    {
        echo "{$this->name} zooming out ...";
    }

    public function videoRecord()
    {
        echo "{$this->name} {$this->quality} videotaping activated ...";
    }
}

// 1st output device
class SLR extends CameraConfigs
{
    use Features;
}

// 2nd output device
class DSLR extends CameraConfigs
{
    use Features;
}

// output device
class RemoteController
{
    public $output;
    public function __construct($output)
    {
        $this->output = $output;
    }

    public function zoomIn()
    {
        $this->output->magnify();
    }

    public function zoomOut()
    {
        $this->output->reset();
    }

    public function record()
    {
        $this->output->videoRecord();
    }
}

(function () {
    $slr = new SLR('SLR', 14, 'HD');
    $dslr = new DSLR('DSLR', 32, 'UHD');
    $slrButtons = new RemoteController($slr);
    $dslrButtons = new RemoteController($dslr);
// SLR results
    $slrButtons->zoomIn();
    $slrButtons->zoomOut();
    $slrButtons->record();
// DSLR results
    $dslrButtons->zoomIn();
    $dslrButtons->zoomOut();
    $dslrButtons->record();
})();


// JavaScript equivalent

echo '



<script>
  // output device
class CameraController {

    constructor(output) {
        this.output = output;
    }

    zoomIn() {
        this.output.magnify();
    }

    zoomOut() {
        this.output.reset();
    }

    record() {
        this.output.videoRecord();
    }

}

// 1st output device
class CameraSLR {
    magnify() {
        console.log("SLR zooming in on 14X ...");
    }

    reset() {
        console.log("SLR zooming out ...");
    }

    videoRecord() {
        console.log("SLR HD videotaping activated ...");
    }
}

// 2nd output device
class CameraDSLR {
    magnify() {
        console.log("DSLR zooming in on 32X ... ");
    }

    reset() {
        console.log("DSLR zooming out ...");
    }

    videoRecord() {
        console.log("DSLR UHD videotaping activated ...");
    }
}

(function operateCamera() {
    const slr = new CameraController(new CameraSLR());
    const dslr = new CameraController(new CameraDSLR());
// SLR results
    slr.zoomIn();
    slr.zoomOut();
    slr.record();
// DSLR results
    dslr.zoomIn();
    dslr.zoomOut();
    dslr.record();
})();


    </script>
    ';

