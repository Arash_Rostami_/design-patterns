<?php


set_error_handler(function ($no, $str, $file, $line) {
    echo "<br><b>[{$no}]</b> {$str} - in {$file} and {$line}, this error was detected." . PHP_EOL;
});

try {


    // FIRST DEVICE
    class Sony
    {
        public function __construct()
        {
            $this->name = 'SONY';
        }

        public function getPrice()
        {
            return '$1\'300';
        }
    }

    // SECOND DEVICE
    class Canon
    {
        public function __construct()
        {
            $this->name = 'CANON';
        }

        public function getPrice()
        {
            return '$3\'200';
        }
    }

// PLAN STRATEGY
    class Sale
    {
        public function setStrategy($device)
        {
            $this->device = $device;
        }

        public function showPrice()
        {
            echo "The price of {$this->device->name} is {$this->device->getPrice()}." . "<br>";
        }
    }


    (function () {
// instantiate products
        $sony = new Sony();
        $canon = new Canon();
//prepare strategy
        $sale = new Sale();
//set strategy
        $sale->setStrategy($sony);
        $sale->showPrice();
//set strategy
        $sale->setStrategy($canon);
        $sale->showPrice();
    })();


    echo
    "
<script>
// FIRST DEVICE
class Sony {
    constructor() {
        this.name = 'SONY';
    }

    getPrice() {
        return '$1400';
    }
}

// SECOND DEVICE
class Canon {
    constructor() {
        this.name = 'Canon';
    }

    getPrice() {
        return '$3200';
    }
}

// PLAN STRATEGY
class Sale {
    setStrategy(device) {
        this.device = device;
    }

    showPrice() {
        return 'The price of ' + this.device.name +' is ' + this.device.getPrice();
    }
}


(function employStrategy() {
// instantiate products
    const canon = new Canon();
    const sony = new Sony();
//prepare strategy
    const sale = new Sale();
//set strategy
    sale.setStrategy(sony);
    console.log(sale.showPrice());
//set strategy
    sale.setStrategy(canon);
    console.log(sale.showPrice());
})();

</script>
    ";

} catch (Exception $e) {

    echo $e->getMessage();
}
