<?php

// Common features of all products
abstract class Cameras
{
    protected $pixel;
    protected $card;

    public function __construct($pixel, $card)
    {
        $this->pixel = $pixel;
        $this->card = $card;
    }

    abstract public function present(): string;
}

// First product
class CameraSLR extends Cameras
{
    public function present(): string
    {
        return "This is a SONY camera with {$this->pixel} px and {$this->card} as memory.";
    }
}

// Second product
class CameraDSLR extends Cameras
{
    public function present(): string
    {
        return "This is a SONY camera with {$this->pixel} px and {$this->card} as memory.";
    }
}

// Building all products
class Sony
{
    public static function create($name, $pixel, $card)
    {
        $camera = [
            "SLR" => new CameraSLR($pixel, $card),
            "DSLR" => new CameraDSLR($pixel, $card)
        ];
        return $camera[$name];
    }
}


(function () {
    //Save product
    $cameras = [];
    //Building process
    array_push($cameras, Sony::create("SLR", 48, "Memory Card"));
    array_push($cameras, Sony::create("DSLR", 82, "Internal Drive"));
    // Showing each product
    foreach ($cameras as $x => $camera) {
        echo $camera->present();
    }
})();

// JavaScript equivalent

echo '



<script>

// First product
class CameraSLR {
    constructor(pixel, card) {
        this.pixel = pixel;
        this.card = card;
    }
    present() {
        console.log(`This is a SONY camera with ${this.pixel} px and
      ${this.card} as memory.`)
    }
}

// Second product
class CameraDSLR {
    constructor(pixel, card) {
        this.pixel = pixel;
        this.card = card;
    }
    present() {
        console.log(`This is a SONY camera with ${this.pixel} px and
      ${this.card} as memory.`)
    }
}

// Building all products
class Sony {
    static create(name, pixel, card) {
        let camera = {
        "SLR"  : new CameraSLR(pixel, card),
        "DSLR" : new CameraDSLR(pixel, card)
        };
        return camera[name];
    }
}


(function makeCamera() {
    //Save product
    const cameras = [];
    //Building process
    cameras.push(Sony.create("SLR", 48, "Memory Card"));
    cameras.push(Sony.create("DSLR", 82, "Internal Drive"));
    // Showing each product
    for (let each in cameras) {
        cameras[each].present()
    }
})();



    </script>
    ';

