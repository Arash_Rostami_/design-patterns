<?php

// Original Object
class Camera
{
    public $brand;

    public function __construct($brand)
    {
        $this->brand = $brand;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function present(): string
    {
        return "This is a camera with {$this->getBrand()} brand.";
    }
}

// Object with additional feature(s)
class DecoratorCamera
{
    public $brand, $model;

    public function __construct($camera, $model)
    {
        $this->camera = $camera;
        $this->model = $model;
    }

    public function getBrand()
    {
        return $this->camera->getBrand();
    }

    public function present(): string
    {
        return "This is a {$this->model} camera with {$this->getBrand()} brand.";
    }
}


(function () {
// Original camera
    $camera = new Camera("SONY");
    print $camera->present();
// The camera with additional feature
    $decoratorCamera = new DecoratorCamera($camera, "DSLR");
    print $decoratorCamera->present();
})();


// JavaScript equivalent

echo '



<script>
  // Original Object
class Camera {
    constructor(brand) {
        this.brand = brand;
    }

    get Brand() {
        return this.brand;
    }

    present() {
        return `This is a camera with ${this.Brand} brand.`
    }
}

// Object with additional feature(s)
class DecoratorCamera {

    constructor(camera, model) {
        this.camera = camera;
        this.model = model;
    }

    get brand() {
        return this.camera.brand;
    }

    present() {
        return `This is a ${this.model} camera with ${this.brand} brand.`
    }
}

(function showResult() {
// Original camera
    const camera = new Camera("SONY");
    console.log(camera.present());
// The camera with additional feature
    const decoratorCamera = new DecoratorCamera(camera, "DSLR");
    console.log(decoratorCamera.present());
})();



    </script>
    ';

