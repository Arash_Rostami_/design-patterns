<?php


class Camera
{
    public $price;

    public function __construct($type, $price)
    {
        $this->type = $type;
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($newPrice)
    {
        $this->price = $newPrice;
    }

    public function accept($newModel)
    {
        return new $newModel($this);
    }
}

// computing different algorithm
class DSLRModel
{
    public function __construct($cam)
    {
        $cam->setPrice($cam->getPrice() * 150 / 100);
    }
}

(function () {
    $camera = new Camera('SLR', 5000);
    echo $camera->getPrice() . PHP_EOL;
    // changing the camera
    $camera->accept('DSLRModel');
    echo $camera->getPrice();
})();


echo
'
<script>

class Camera {
    constructor(name, price) {
        this.name = name;
        this.price = price;
    }

    getPrice() {
        return this.price;
    }

    setPrice(newPrice) {
        this.price = newPrice;
    }

    accept(newModel) {
        return new newModel(this)
    }
}
// computing different algorithm
class AnotherModel {
    constructor(cam) {
        cam.setPrice(cam.getPrice() * 150 / 100);
    }
}

(function testVisitor() {
    const camera = new Camera("CANON", 5000);
    console.log(camera.getPrice());
    // changing the camera
    camera.accept(AnotherModel);
    console.log(camera.getPrice());
})();




</script>
';
