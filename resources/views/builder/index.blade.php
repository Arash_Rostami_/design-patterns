<?php

// Building
class Sony
{
    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    public function make()
    {
        $this->builder->startPhaseOne();
        $this->builder->startPhaseTwo();
        $this->builder->startPhaseThree();
        return $this->builder->get();
    }
}

// Controlling building order
class CameraMaker
{
    private $maker;

    public function startPhaseOne()
    {
        $this->maker = new Camera();
    }

    public function startPhaseTwo()
    {
        $this->maker->makeLens();
    }

    public function startPhaseThree()
    {
        $this->maker->makeConfig();
    }

    public function get()
    {
        return $this->maker->present();
    }
}

// All of tiny details of building
class Camera
{
    private $part = [];

    public function makeLens()
    {
        array_push($this->part, "SLR");
    }

    public function makeConfig()
    {
        array_push($this->part, "Digital");
    }

    public function present()
    {
        echo "This is a SONY camera that is made with the lens of
         {$this->part[0]} and {$this->part[1]} configurations.";
    }
}


(function () {
    //  Preparing the builder
    $maker = new CameraMaker();
    // Activating the building
    $sony = new Sony($maker);
    // Checking the building process
    return ($sony) ? $sony->make() : "SONY camera is not made";
})();


// JavaScript equivalent

echo '



<script>
    // Building
class Sony {
    constructor(builder) {
        builder.startPhaseOne();
        builder.startPhaseTwo();
        builder.startPhaseThree();
        return builder.get();
    }

}

// Controlling building order
class CameraMaker {
    constructor() {
        this.maker = [];
    }

    startPhaseOne() {
        this.maker = new Camera()
    }

    startPhaseTwo() {
        this.maker.makeLens();
    }

    startPhaseThree() {
        this.maker.makeConfig();
    }

    get() {
        return this.maker;
    }
}

// All of tiny details of building
class Camera {
    constructor() {
        this.part = [];
    }

    makeLens() {
        this.part.push("SLR");
    }

    makeConfig() {
        this.part.push("Digital");
    }

    present() {
        console.log(`This is a SONY camera that is made with the lens of
    ${this.part[0]} and ${this.part[1]} configurations.`);
    }
}


(function build() {
// Preparing the builder
    const maker = new CameraMaker();
// Activating the building
    const sony = new Sony(maker);
// Checking the building process
    return(sony) ? sony.present() : "SONY camera is not made";
})();


    </script>
    ';

