<?php


// checklist class one
class Age
{
    public function get($age): bool
    {
        return ($age < 50);
    }
}

// checklist class two
class Education
{
    public function get($degree): bool
    {
        return ($degree === "diploma");
    }
}

// checklist class three
class Experience
{
    public function get($year): bool
    {
        return ($year >= 7);
    }
}

// automatic assessment class
class Immigration
{
    public $applicant;

    public function __construct($applicant)
    {
        $this->applicant = $applicant;
    }

    public function check($age, $degree, $year): string
    {
        $result = "eligible";
        if (
            !(new Age())->get($age) ||
            !(new Education())->get($degree) ||
            !(new Experience())->get($year)
        ) {
            $result = "not eligible";
        }
        return "{$this->applicant} is {$result}.";
    }
}

(function () {
    //define applicant
    $subject = new Immigration("Arash");
    // assess user
    echo $subject->check(40, "diploma", 10);
})();


// JavaScript equivalent

echo '



<script>

// checklist class one
class Age {
    get(age) {
        return (age < 50);
    }
}
// checklist class two
class Education {
    get(degree) {
        return (degree === "diploma");
    }
}

// checklist class three
class Experience {
    get(year) {
        return (year >= 7);
    }
}

// automatic assessment class
class Immigration {

    constructor(applicant) {
        this.applicant = applicant;
    }

    check(age, degree, year) {
        let result = "eligible";

        if (
            !new Age().get(age) ||
            !new Education().get(degree) ||
            !new Experience().get(year)
        ) {
            result = "not eligible";
        }
        return `${this.applicant} is ${result}.`;
    }
}



(function () {
//define applicant
const subject = new Immigration("Arash");
// assess user
const result = subject.check(40, "diploma", 10);
// print result
console.log(result);
})();



    </script>
    ';

