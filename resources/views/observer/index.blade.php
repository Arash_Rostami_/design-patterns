<?php


class Observe
{
    public $observers;

    public function __construct()
    {
        $this->observers = array();
    }

    public function subscribe($fn): int
    {
        return array_push($this->observers, $fn);
    }

    public function unsubscribe($fn): array
    {
        return $this->observers = array_filter($this->observers,
            function ($eachFn) use ($fn) {
                return $eachFn != $fn;
            });
    }

    public function fire()
    {
        foreach ($this->observers as $observer) {
            echo $observer;
        }
    }
}

abstract class Event
{
    public $message;

    public function __toString(): string
    {
        return $this->message;
    }
}

class EventOne extends Event
{
    public function __construct()
    {
        $this->message = "The first event triggered!";
    }
}


class EventTwo extends Event
{
    public function __construct()
    {
        $this->message = "The second event triggered!";
    }
}


(function () {
    $observer = new Observe();
    // add events
    $observer->subscribe(new EventOne());
    $observer->subscribe(new EventTwo());
    // trigger events
    $observer->fire();
    // remove events
    $observer->unsubscribe(new EventTwo());
    $observer->fire();
})();


// JavaScript equivalent

echo '



<script>

// Saving all events
class EventObserver {
    constructor() {
        this.observers = [];
    }

    subscribe(fn) {
        this.observers.push(fn);
    }

    unsubscribe(fn) {
        this.observers = this.observers.filter(EachFn => EachFn !== fn);
    }

    fire() {
        this.observers.forEach(observer => observer());
    }
}

function EventOne() {
    console.log("First button clicked!");
}

function EventTwo() {
    console.log("Second button clicked!");
}


(function observeEvents() {
    const observe = new EventObserver();

//    // add events
    observe.subscribe(EventOne);
    observe.subscribe(EventTwo);
    // trigger events
    observe.fire();
    // remove events
    observe.unsubscribe(EventTwo);
    observe.fire();
})();




</script>
    ';

