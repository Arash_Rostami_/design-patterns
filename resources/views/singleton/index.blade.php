<?php


// The main Product
class Camera
{
    public function __construct($name)
    {
        $this->name = $name;
    }
}

// Creating another object from Camera
class CameraMaker
{
    static $instance;

    public static function createInstance(): Camera
    {
        return new Camera("SONY");
    }

    public static function getInstance(): Camera
    {
        if (!self::$instance) {
            self::$instance = self::createInstance();
        }
        return self::$instance;
    }
}

(function () {
    //creating the 1st instance of camera
    $cameraMakerOne = CameraMaker::getInstance();
    //creating the 2nd instance of camera
    $cameraMakerTwo = CameraMaker::getInstance();
    // comparing instances
    echo ($cameraMakerOne === $cameraMakerTwo) ? "Yay, similar :)" : "No way!";
})();


// JavaScript equivalent

echo '



<script>

// The main Product
class Camera {
    constructor(name) {
        this.name = name;
    }
}

// Creating another object from Camera
class CameraMaker {
    static instance;

    static createInstance() {
        return new Camera("SONY");
    }

    static getInstance() {
        if (!CameraMaker.instance) {
            CameraMaker.instance = CameraMaker.createInstance();
        }
        return CameraMaker.instance;
    }
}

(function makeCamera() {
    //creating the 1st instance of camera
    const CameraMakerOne = CameraMaker.getInstance();
    //creating the 2nd instance of camera
    const CameraMakerTwo = CameraMaker.getInstance();
    // comparing instances
    console.log((CameraMakerOne === CameraMakerTwo) ? "Yay, similar :)" : "No way!");
})();



</script>
    ';

