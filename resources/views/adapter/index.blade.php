<?php

// Api connection for Geolocation
class mapApiConnection
{
    public function login($apiKeys)
    {
        // ...
    }

    public function getLocation($ip): string
    {
        // ... $ip ...
        return "Japan";
    }

    public function nameServer($isp): string
    {
        // ... $isp ...
        return "At&T";
    }
}

// Prepare user interface
class mapAdapter
{
    public function __construct($apiKeys)
    {
        $this->apiKeys = $apiKeys;
        $this->mapApi = new mapApiConnection();
        $this->mapApi->login($this->apiKeys);
    }

    public function request($ip, $isp)
    {
        echo $this->mapApi->nameServer($isp)
            . " from " . $this->mapApi->getLocation($ip);
    }
}


(function () {
// credentials
    $apiKeys = [];
// connect
    $map = new mapAdapter($apiKeys);
// present data
    $map->request("#testIp", "#testIsp");
})();



// JavaScript equivalent

echo '



<script>

// Api connection for Geolocation
class mapApiConnection {
    login(apiKeys) {
        // ...
    }

    getLocation(ip) {
        // ... ip ...
        return "Japan";
    }

    nameServer(isp) {
        // ... isp ...
        return "At&T";
    }
}

// Prepare user interface
class mapAdapter {
    constructor(apiKeys) {
        this.apiKeys = apiKeys;
        this.mapApi = new mapApiConnection();
        this.mapApi.login(this.apiKeys);
    }

    request(ip, isp) {
        return `${this.mapApi.nameServer(isp)} from ${this.mapApi.getLocation(ip)}`;
    }
}


(function () {
// credentials
    const apiKeys = {user: "admin", pass: "test"};
// connect
    const map = new mapAdapter(apiKeys);
// present data
    console.log(map.request("#testIp", "#testIsp"));
})();



    </script>
    ';

