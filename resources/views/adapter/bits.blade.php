<?php

ob_start();
?>
<h1> HELLO BUFFERING CONTENT #1</h1>

<?php
$content = ob_get_contents();
ob_end_clean();

echo $content . PHP_EOL;
?>

<?php
ob_start();
?>
<h1> HELLO BUFFERING CONTENT #2</h1>

<?php
$content = ob_get_clean();
echo $content . PHP_EOL;
?>


<?php
ob_start();
?>
<h1> HELLO BUFFERING CONTENT #3</h1>

<?php
$content = ob_get_flush();
echo PHP_EOL;
?>


<?php
ob_start();
?>
<h1> HELLO BUFFERING CONTENT #4</h1>

<?php
//ob_end_clean();
ob_end_flush();
echo PHP_EOL;
echo '++++++++++++++++++++++++++++++++' . PHP_EOL;
?>


<?php
$number = 3;

set_error_handler(function ($no, $str, $file, $line) {
    echo "<br><b>[{$no}]</b> {$str} - in {$file} and {$line}, this error was detected." . PHP_EOL;
});

if ($number < 5) {
    trigger_error('OMG!' . $number . ' cannot be larger than three!');

}
restore_error_handler();

?>
<?php
$digit = 30;

//set_exception_handler(function ($exception) {
//    echo "<br><b>{$exception->getMessage()}</b> " . PHP_EOL;
//});

//if ($digit > 10) {
//    throw new Exception('OMG!'.$number .' should be larger than three!');
//};
//restore_exception_handler();

try {
    if ($digit > 10) {
        throw new Exception('OMG!' . $number . ' should be larger than three!');
    };
} catch (Exception $e) {
    echo "<br><b>{$e->getMessage()}</b> \n" . PHP_EOL;
    echo '<br>';
};

?>



<?php
//$digit = 30;
//
//if ($digit > 10) {
//    error_log('NO, it just cannot be!!!', 1, "arashrostami@time-gr.com");
//};


?>



<?php


// $to = "arashrostami@time-gr.com";

//$msg = "با سلام کاربر محترم، برای تهیه مقدمات تمدید سایت خود در اسرع وقت وارد سی پنل  خود شوید. باتشکر پشتیانی سرور پارس";
//
//// use wordwrap() if lines are longer than 70 characters
//$msg = wordwrap($msg, 70);
//
//$headers = array(
//    "From" => "Server Pars<support.client@parserver>",
//    "Bcc" => 'education@time-gr.com',
//    // "Cc" => 'support@selfstudyielts.com'
//);
//
//// send email = first is $to
//mail("", "اطلاعیه مهم", $msg, $headers);


// OR version II with implode
//$headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
//$headers[] = 'From: Birthday Reminder <birthday@example.com>';
//$headers[] = 'Cc: birthdayarchive@example.com';
//$headers[] = 'Bcc: birthdaycheck@example.com';
//
//// Mail it
//mail("", "title", $msg, implode("\r\n", $headers));





?>

<?php
// filter_var(2) | filter_input(3) | filter_has_var(2)


$filter = "aras<br>hrostami<br>";

// for input_ger/post
echo (filter_has_var(INPUT_GET, $filter)) ? 'It is input' : 'No input';

echo "<br>";

// sanitize input
echo (filter_input(INPUT_GET, $filter, FILTER_SANITIZE_STRING)) ? "Sanitized" : "NOT SANITIZED";

//for multiple filter_input_array(INPUT_GET, $filters)
//$filters = array(
//    "age" => array(
//        "filter" => FILTER_VALIDATE_INT,
//        "options" => array("min_range" => 1, "max_range" => 120)
//    ),
//    "email" => FILTER_VALIDATE_EMAIL
//);

echo "<br>";


// sanitize variable
$cleaned = filter_var($filter, FILTER_SANITIZE_STRING);

//for multiple filter_var_array($filters)
//$filters = array(
//    "name" => 'something',
//    "email" => FILTER_VALIDATE_EMAIL
//);


echo ($cleaned) ? ("$cleaned is a valid name") : ("$cleaned is not a valid name");




?>


<?php

echo "-------------------------------------------" . "\r\n";

class ErrorCheck
{
    public static function showMessage($errno, $errstr, $errfile, $errline)
    {
        echo "<br><b>Error:</b> [$errno] $errstr<br>";

        echo " Error on line $errline in $errfile<br>";
    }

    public static function __callStatic($fn, $arg)
    {
        set_error_handler(['ErrorCheck', 'showMessage']);

        return self::run($fn, $arg);
    }

    public static function run($fn, $arg)
    {
        try {

            return $fn(...$arg);
        } catch (Exception $e) {

            echo $e->getMessage();
        }
    }
}


function divide($num)
{
    return $num / 0;
}

ErrorCheck::divide(2);



?>


<?php

//
//if (!file_exists("test.txt")) {
//    $file = fopen("test.txt", "w") or die("Unable to open file!");
////    error_log("A file has been opened", 1, "education@time-gr.com",
////        "From: TIME Educational Group");
//}
file_put_contents("test.txt", 'Arash Rostami|', FILE_APPEND);
$content = file_get_contents("test.txt");

$count = substr_count($content, 'Arash Rostami');

//fclose($file);


// echo $content;
settype($content, 'array');
print_r($count);
print_r($content);
echo "<br>";
?>

<?php

date_default_timezone_set('Asia/Tehran');
echo date('H:i:s - a');
echo "<br>";
?>

<?php

class Privacy
{
    public $propertyOne = 'I am public';
    protected $propertyTwo = 'I am protected';
    private $propertyThree = 'I am private';
}

$privacy = new Privacy();
$instance = new ReflectionClass('privacy');
foreach ($instance->getDefaultProperties() as $k => $v) {
    echo "$k is key for $v" . " variable<br>";
};
var_export($instance->getProperties(ReflectionProperty::IS_PRIVATE));

echo "<br>";


?>

<?php


// PHP program to merge two objects

class Geeks {
    // Empty class
}

$objectA = new Geeks();
$objectA->a = 1;
$objectA->b = 2;
$objectA->d = 3;

$objectB = new Geeks();
$objectB->d = 4;
$objectB->e = 5;
$objectB->f = 6;

//settype($objectA, "array");settype($objectB, "array");
//$obj_merged = (object) array_merge($objectA,$objectB);

        //OR
$obj_merged = (object) array_merge(
    (array) $objectA, (array) $objectB);


print_r($obj_merged);
echo "<br>";
print_r($obj_merged->f);
echo "<br>";


$obj = (object) array('arash' => 'foo');
var_dump(($obj->{'arash'})); // outputs 'bool(true)' as of PHP 7.2.0; 'bool(false)' previously
var_dump(key($obj)); // outputs 'string(1) "1"' as of PHP 7.2.0; 'int(1)' previously





?>
