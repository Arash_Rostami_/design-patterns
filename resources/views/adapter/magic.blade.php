<?php

class Test
{


    protected $prop = "W3Schools";

    public $propClone = "NOT CLONED";

    protected static $staticProp = "A.R.";

    // test methods
    protected static function Public()
    {
        return "static public fn";
    }

    private function CallPublic()
    {
        return "public fn";
    }

    // change property if protected or private
    public function __set($prop, $value)
    {
        $this->prop = $value;
    }

    // get property if protected or private
    public function __get($prop)
    {
        return $this->prop;
    }

    // show if isset is executed
    public function __isset($prop)
    {
        echo "It is set but you are not allowed to view it by typing {$prop}", PHP_EOL;
    }

    // if a method is called
    public function __call($func, $args)
    {
        return $this->$func();
    }

    // if a static method is called
    public static function __callStatic($func, $args)
    {
        echo "calling static that is private! ";
    }

    // to be run in case of clone
    public function __clone()
    {
        $this->prop = 'Arash';
        $this->propClone = 'CLONED';
    }

    // ban var_dump or print_r
    public function __debugInfo()
    {
        echo 'This is completely private: you cannot access the ';
    }

    // act like a method or second constructor
    public function __invoke(...$arg)
    {
        print_r(func_get_args());
    }
}

$test = new Test();
echo $test->prop, PHP_EOL;
$test2 = clone $test;
echo $test2->propClone, PHP_EOL;
$test([1, 2, 3], "hello");
var_dump($test);
echo $test->CallPublic(), PHP_EOL;
echo Test::Public(), PHP_EOL;
isset($test->prop);
$test->prop = "Arash Rostami";
echo $test->$prop;
