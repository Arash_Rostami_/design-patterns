<?php

function br()
{
    return (PHP_SAPI === 'cli' ? "\n" : "<br />");
}

function print_answer($answer, $expected_answer, $example_number)
{
    $result = $answer;

    // if overall is higher: not defined in Q
    if (array_sum($result) > $expected_answer) {
        echo "Not defined in the question!!!";
    }
    // if overall score is the same
    if (array_sum($result) == $expected_answer) {
        $answer = showAnswer($result);
    }
    // if overall score is the higher
    if (array_sum($result) < $expected_answer) {
        list($answer) = modifyAnswer($expected_answer, $result);
    }

    //test function according to helper given
    testAnswer($example_number, $answer, $expected_answer);
}

// UNDER THE HOOD
/**
 * @param $result
 * @return float|int
 */
function showAnswer($result)
{
    echo "value: " . array_sum($result) . ' - ';
    return array_sum($result);
}


/**
 * @param $expected_answer
 * @param $result
 * @return array
 */
function modifyAnswer($expected_answer, $result): array
{
    // count extra to add to each score of question based on expected result
    $score = countExtra($expected_answer, $result);
    // prepare parameters to send into array_map in form of array
    $new_score = makeParam($result, $score);


    // map the parameters and edit marks of questions
    $final_score = array_map(function ($item, $param) {
        return ($item + $param);
    }, $result, $new_score);

    //additional data - just to show the final score
    echo "value: " . array_sum($final_score) . ' - ';
    return array(array_sum($final_score));
}

/**
 * @param $result
 * @param $score
 * @return array
 */
function makeParam($result, $score)
{
    return array_pad([$score], count($result), $score);
}

/**
 * @param $expected_answer
 * @param $result
 * @return float|int
 */
function countExtra($expected_answer, $result)
{
    return ($expected_answer - array_sum($result)) / count($result);
}


/**
 * @param $example_number
 * @param int $answer
 * @param $expected_answer
 */
function testAnswer($example_number, int $answer, $expected_answer): void
{
    echo($example_number . ") ");
    if ($answer === $expected_answer) {
        echo("Correct" . br());
    } else {
        echo("Incorrect" . br());
    }
    echo(br());
}

function task2($input)
{
    $result = array_sum($input);
    $i = 0;
    while ($i < count($input)) {
        ($input[$i] > ++array_keys($input)[$i])
            ? $result += $input[$i] - ++array_keys($input)[$i]
            : $result += 0;
        $i++;
    }
    print_r($result);
}

task2([1, 2, 3, 4, 5]);
task2([1, 4, 5, 4, 5]);



//print_answer([1, 2, 3, 4, 5], 15, 1);
// expected answer : 15

//print_answer([1, 4, 5, 4, 5], 23, 2);

// expected answer : 23


class Math
{
    protected $result, $score;
    protected $new_score = [];
    protected $final_score = [];

    public function print_answer($answer, $expected_answer, $example_number)
    {
        $this->result = $answer;

        // if overall is higher: not defined in Question
        if (array_sum($this->result) > $expected_answer) {
            list($expected_answer, $answer) = $this->showNotDefined($expected_answer);
        }
        // if overall score is the same
        if (array_sum($this->result) == $expected_answer) {
            $answer = $this->showAnswer($this->result);
        }
        // if overall score is the higher
        if (array_sum($this->result) < $expected_answer) {
            list($answer) = $this->modifyAnswer($expected_answer, $this->result);
        }

        //test function according to helper given
        $this->testAnswer($example_number, $answer, $expected_answer);
    }

    /**
     * @param int $expected_answer
     * @return int[]
     */
    public function showNotDefined(int $expected_answer): array
    {
        $answer = $expected_answer = 0;
        echo "Not defined in the question!!! ";
        return array($expected_answer, $answer);
    }

    /**
     * @param $result
     * @return float|int
     */
    public function showAnswer($result)
    {
        return array_sum($result);
    }

    /**
     * @param $expected_answer
     * @param $result
     * @return array
     */
    function modifyAnswer($expected_answer, $result): array
    {
        // count extra to add to each score of question based on expected result
        $score = $this->countExtra($expected_answer, $result);
        // prepare parameters to send into array_map in form of array
        $new_score = $this->makeParam($result, $score);

        function changeMark($item, $param)
        {
            return ($item + $param);
        }

        // map the parameters and edit marks of questions
        $final_score = array_map("changeMark", $result, $new_score);

        return array(array_sum($final_score));
    }

    /**
     * @param $result
     * @param $score
     * @return array
     */
    public function makeParam($result, $score)
    {
        return array_pad([$score], count($result), $score);
    }

    /**
     * @param $expected_answer
     * @param $result
     * @return float|int
     */
    public function countExtra($expected_answer, $result)
    {
        return ($expected_answer - array_sum($result)) / count($result);
    }

    /**
     * @param $example_number
     * @param int $answer
     * @param $expected_answer
     */
    public function testAnswer($example_number, int $answer, $expected_answer): void
    {
        echo($example_number . ") ");

        echo ($answer === $expected_answer)
            ? ("Correct" . $this->br())
            : ("Incorrect" . $this->br());

        echo($this->br());
    }

    /**
     * @return string
     * go to the next line
     */
    public function br(): string
    {
        return (PHP_SAPI === 'cli' ? "\n" : "<br />");
    }
}

//
//(new Math())->print_answer([1, 2, 3, 4, 5], 15, 1);
//(new Math())->print_answer([1, 4, 5, 4, 5], 23, 2);





//TASK 3


function print_answer_task3($line, $expected_answer, $example_number)
{

    // creating two classes based on IQ levels and standing order in array
    list($top_iq, $low_iq, $first_index) = compareIQs($line);

    // giving priority to the first person standing in array
    $low_iq = prioritizeFirstPerson($line[0], $low_iq);

    //creating order of those standing in low IQ category
    $final_low_iq = createLowIQorder($low_iq, $first_index);

    // merging the number of those who are ahead of each candidate in array in each category
    $answer = getNumberInQueue($line, $top_iq, $final_low_iq);

    // showing result if need be
    //  print_r($answer);
    testResponseByHelper($example_number, $answer, $expected_answer);
}

;


// Under the hood

/**
 * @param array $line
 * @return array[]
 */
function compareIQs(array $line): array
{
    $first_index = $line[0];
    $top_iq = array($first_index);
    $low_iq = array($first_index);

    for ($i = 0; $i < count($line) - 1; ++$i) {
        ($line[$i] < $line[$i + 1])
            ? array_push($top_iq, $line[$i + 1])
            : array_push($low_iq, $line[$i + 1]);
    }

    return array($top_iq, $low_iq, $first_index);
}

/**
 * @param $line
 * @param array $low_iq
 * @return array
 */
function prioritizeFirstPerson($line, array $low_iq): array
{
    $pos = array_search($line, $low_iq);
    unset($low_iq[$pos]);
    return $low_iq;
}


/**
 * @param array $low_iq
 * @param $first_index
 * @return array
 */
function createLowIQorder(array $low_iq, $first_index): array
{
    $low_iq_keys = sortIndex($low_iq, $first_index);
    $low_iq_values = array_values($low_iq);
    return array_combine($low_iq_keys, $low_iq_values);
}


/**
 * @param array $low_iq
 * @param $first_index
 * @return array
 */
function sortIndex(array $low_iq, $first_index): array
{
    $low_iq_keys = array();

    foreach ($low_iq as $key => $val) {
        ($first_index > $val)
            ? array_push($low_iq_keys, ++$key)
            : array_push($low_iq_keys, $key);
    };
    return $low_iq_keys;
}

/**
 * @param array $line
 * @param array $top_iq
 * @param array $final_low_iq
 * @return array
 */
function getNumberInQueue(array $line, array $top_iq, array $final_low_iq): array
{
    $result = array();
    foreach ($line as $index) {
        if (in_array($index, $top_iq)) {
            array_push($result, 0);
        }

        if (in_array($index, $final_low_iq)) {
            $result[] = array_search($index, $final_low_iq);
        }
    }
//    print_r($result);
    return $result;
}

/**
 * @param $example_number
 * @param array $answer
 * @param $expected_answer
 */
function testResponseByHelper($example_number, array $answer, $expected_answer): void
{
    echo($example_number . ") ");
    echo ($answer === $expected_answer)
        ? ("Correct" . br())
        : ("Incorrect" . br());
    echo(br());
}


// Check the result
//print_answer_task3([124, 129, 125, 123, 122, 130], [0, 0, 1, 3, 4, 0], 1);
// expected answer : [0,0,1,3,4,0]

//print_answer_task3([123, 124, 121, 125, 122], [0, 0, 2, 0, 3], 2);
// expected answer : [0,0,2,0,3]




function task_3($input)
{
    $result = [];
    for ($a = 0; $a < count($input); $a++) {
        array_push($result, 0);
    };
    $i = 0;
    while ($i < count($input)) {
        $j = 0;
        while ($j < $i) {
            if ($input[$i] < $input[$j]) {
                $result[$i] += 1;
            };
            $j++;
        };

        $i++;
    };

    print_r($result);
}


task_3([124, 129, 125, 123, 122, 130]);

task_3([123, 124, 121, 125, 122]);



