<?php


class Iterate
{
    public $items, $count;

    public function __construct($items)
    {
        $this->count = 0;
        $this->items = $items;
    }

    public function next()
    {
        echo $this->items[$this->count++];
    }

    public function hasNext(): bool
    {
        return $this->count < sizeOf($this->items);
    }
}


(function () {
    // setting up iterator
    $items = ["SONY", "Camera", "SLR", "72px"];
    $iterator = new Iterate($items);
    // counting items
    while ($iterator->hasNext()) {
        echo $iterator->next();
    }
})();





// JavaScript equivalent

echo '



<script>

class Iterator {
    constructor(item) {
        this.count = 0;
        this.items = item;
    }

    next() {
        return this.items[this.count++];
    }

    hasNext() {
        return this.count < this.items.length;
    }
}


(function countIterator() {
    // setting up iterator
    const items = ["SONY", "Camera", "SLR", "72px"];
    const iterator = new Iterator(items);
    // counting items
    while (iterator.hasNext()) {
        console.log(iterator.next());
    }
})();

    </script>
    ';

