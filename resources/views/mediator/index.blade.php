<?php

//Member class
class User
{
    public $whatsapp;

    public function __construct($name)
    {
        $this->name = $name;
        $this->whatsapp = null;
    }

    // To send through Mediator class
    public function send($message, $to)
    {
        return $this->whatsapp->send($message, $this, $to);
    }

    public function receive($message, $from)
    {
        echo "{$from->name} to {$this->name} : {$message} \r\n";
    }
}

// Mediator Class
class WhatsApp
{
    public $users;

    public function __construct()
    {
        $this->users = [];
    }

    // To link members
    public function addMember($user)
    {
        $this->users[$user->name] = $user;
        return $user->whatsapp = $this;
    }

    // Using constructors saved array to send through items class
    public function send($message, $from, $to)
    {
        if (!is_array($to))
            return $this->users[$to->name]->receive($message, $from);

        foreach ($to as $user)
            $this->users[$user->name]->receive($message, $from);
    }
}


(function () {
    // Defining Mediator
    $whatsapp = new WhatsApp();
    // Defining Members
    $ARASH = new User('Arash');
    $PARISA = new User('Parisa');
    $SARA = new User('Sara');

    $whatsapp->addMember($ARASH);
    $whatsapp->addMember($PARISA);
    $whatsapp->addMember($SARA);


    $ARASH->send('Hey, what\'s happening?', $PARISA);
    $PARISA->send('Hey, not much, you?', $ARASH);
    $SARA->send('Hey guys, what about me!', [$ARASH, $PARISA]);
})();


// JavaScript equivalent

echo '



<script>
//Member class
class User {
    constructor(name) {
        this.name = name;
        this.whatsapp = null;
    }
    // To send through Mediator class
    send(message, to) {
        this.whatsapp.send(message, this, to);
    }

    receive(message, from) {
        console.log(`${from.name} to ${this.name} : ${message}`);
    }
}

// Mediator Class
class WhatsApp {
    constructor() {
        this.users = [];
    }
    // To link members
    addMember(user) {
        this.users[user.name]=user;
        user.whatsapp = this;
    }
    // Using constructors saved array to send through items class
send(message, from, to) {
    if (!Array.isArray(to)) {
        return this.users[to.name].receive(message, from);
    }

    for (let user of to) {
        this.users[user.name].receive(message, from);
    }
    }
}


(function RunWhatsApp() {
    // Defining Mediator
    const whatsapp = new WhatsApp();
    // Defining Members
    const ARASH = new User("Arash");
    const PARISA = new User("Parisa");
    const SARA = new User("Sara");

    whatsapp.addMember(ARASH);
    whatsapp.addMember(PARISA);
    whatsapp.addMember(SARA);

    ARASH.send("Hey, what\'s happening?", PARISA);
    PARISA . send("Hey, not much, you?", ARASH);
    SARA . send("Hey guys, what about me!", [ARASH, PARISA]);
})()


</script >
';

